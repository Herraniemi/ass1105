FROM node:18-alpine AS builder
COPY ./*.json ./
COPY ./src ./src
RUN npm ci
RUN npm run build

FROM node:18-alpine AS final
WORKDIR /app
COPY package*.json ./
COPY --from=builder ./dist ./dist
RUN npm ci --production

EXPOSE 3000
CMD ["npm", "start"]

